// Create the HTML table & logo picture that are injected to document.
var imagesPath = chrome.extension.getURL('images'); 
table = $('<div class="bigContainer"> <div class="aboveTable"> <div id="aboveHeader">AutoDS Item Grabber <a href="https://intercom.help/autods/faq-english/autods-item-grabber" target="_blank">Help</a></div> </div> <div class="tableContainer"> <div style="display:none;" id="content-spinner-small"><img src="'+imagesPath+'/loading.gif"></div> <table id="grabberTable" class="panel-body"> <!-- <thead> <tr> <th>Product ID</th> <th>Title</th> <th>Price</th> <th>Ranking</th> <th>Stars</th> </tr> </thead> --> <tbody> <tr></tr> </tbody> </table> </div> <div class="belowTable"> <div id="counter" class="counter">Products: 0</div> <div> <button id="bulkGrab" class="styleButtons">Extract</button> <button id="deleteGrab" class="styleButtons">Delete</button> </div> <div> <button id="exportToDS" class="styleButtons">Export as CSV</button> <!-- <button id="exportToDS" class="styleButtons" onclick="downloadCSV();">Multi Extractor</button> --> </div> </div> </div>');
grabber = $('<div> <img id="grabberDS" src="https://i.imgur.com/5DSL9ji.png"></img> </div>');
$('body').append(grabber);
$('body').append(table);

// Create, read and delete cookie functions
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	} else {
		expires = "";
	}
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

// Download CSV function
function convertArrayOfObjectsToCSV(args) {  
	var result, ctr, keys, columnDelimiter, lineDelimiter, data;

	data = args.data || null;
	if (data == null || !data.length) {
		return null;
	}

	columnDelimiter = args.columnDelimiter || ',';
	lineDelimiter = args.lineDelimiter || '\n';

	keys = Object.keys(data[0]);

	result = '';
	result += keys.join(columnDelimiter);
	result += lineDelimiter;

	data.forEach(function(item) {
		ctr = 0;
		keys.forEach(function(key) {
			if (ctr > 0) result += columnDelimiter;

			result += item[key];
			ctr++;
		});
		result += lineDelimiter;
	});

	return result;
}
// Download CSV function
function downloadCSV(args) {

	var data, filename, link;
	for (i=0;i<productListExport.length;i++){
		if (productListExport[i].length > 0){
			var csv = convertArrayOfObjectsToCSV({
				data: productListExport[i]
			});
			console.log(productListExport);
			// This is exporting the file by supplier name. Supplier "i" number is by their position in suppliers list (var productListExport)
			if (csv == null) return;
			// filename = args.filename || 'export.csv';
			if (i == 0) {
				filename = 'Amazon US Export.csv';
			} else if (i == 1) {
				filename = 'Amazon UK Export.csv';
			} else if (i == 2) {
				filename = 'Amazon DE Export.csv';
			} else if (i == 3) {
				filename = 'Banggood Export.csv';
			} else if (i == 4) {
				filename = 'Aliexpress Export.csv';
			} else if (i == 5) {
                filename = 'Walmart Export.csv';
            } else if (i == 6) {
                filename = 'HomeDepot Export.csv';
            } else if (i == 7) {
                filename = 'Amazon IT Export.csv';
            } else if (i == 8) {
                filename = 'Amazon FR Export.csv';
            } else if (i == 9) {
                filename = 'Wayfair UK Export.csv';
            } else if (i == 10) {
                filename = 'Wayfair COM Export.csv';
            } else if (i == 11) {
                filename = 'costco COM Export.csv';
            } else if (i == 12) {
                filename = 'overstock COM Export.csv';
            } else if (i == 13) {
                filename = 'Chinabrands COM Export.csv';
            } else if (i == 14) {
                filename = 'Costway COM Export.csv';
            } else if (i == 15) {
                filename = 'DHGate COM Export.csv';
            } else {
				filename = 'export.csv';
			}

			if (!csv.match(/^data:text\/csv/i)) {
				// csv = 'data:text/csv;charset=utf-8,' + csv;
				csv = 'data:text/csv;charset=UTF-8,' + '\uFEFF' + csv;
			}
			data = encodeURI(csv);

			link = document.createElement('a');
			link.setAttribute('href', data);
			link.setAttribute('download', filename);
			link.click();
		}
	}
}

// Gets the source site on page load to assign "Grab Products" button the correct function for each site.
function getSourceSite(){
	if (document.URL.includes('amazon.com')) {
		sourceSite = 'Amazon US';
	} else if (document.URL.includes('amazon.co.uk')) {
		sourceSite = 'Amazon UK';
	} else if (document.URL.includes('amazon.de')) {
		sourceSite = 'Amazon DE';
	} else if (document.URL.includes('www.banggood')) {
		sourceSite = 'Banggood';
	} else if (document.URL.includes('us.banggood')) {
		sourceSite = 'Banggood US';
	} else if (document.URL.includes('usa.banggood')) {
		sourceSite = 'Banggood US';
	} else if (document.URL.includes('aliexpress')) {
		sourceSite = 'Aliexpress';
	} else if (document.URL.includes('walmart')) {
		sourceSite = 'Walmart';
	} else if (document.URL.includes('homedepot')) {
        sourceSite = 'HomeDepot';
    } else if (document.URL.includes('amazon.it')) {
        sourceSite = 'Amazon IT';
    } else if (document.URL.includes('amazon.fr')) {
        sourceSite = 'Amazon FR';
    } else if (document.URL.includes('wayfair.co.uk')) {
        sourceSite = 'Wayfair UK';
    } else if (document.URL.includes('wayfair.com')) {
        sourceSite = 'Wayfair COM';
    } else if (document.URL.includes('.costco.com')) {
        sourceSite = 'Costco COM';
    } else if (document.URL.includes('.chinabrands.com')) {
        sourceSite = 'Chinabrands COM';
    } else if (document.URL.includes('.costway.com')) {
        sourceSite = 'Costway COM';
    } else if (document.URL.includes('.dhgate.com')) {
        sourceSite = 'DHGate COM';
    } else if (document.URL.includes('.overstock.com')) {
        sourceSite = 'overstock COM';
    } 
    
	console.log('sourceSite: ' + sourceSite);
}

// Get the products from saved local storage on every page refresh / new page.
function getProductList(){
	chrome.storage.local.get('productList', function (items) {
		if (items.productList != null) {
			productList = items.productList;
			var table = document.getElementById("grabberTable").getElementsByTagName('tbody')[0];
			for (i=0;i<productList.length;i++){
				var row = table.insertRow(-1);
				var cell1 = row.insertCell(0).innerHTML = '<a href='+productList[i].URL+' target="_blank">'+productList[i].ID+'</a>';
			}
			$('#counter').get(0).innerText="Products: "+productList.length;
		}
	});
	chrome.storage.local.get('productListExport', function (items) {
		if (items.productListExport != null) {
			productListExport = items.productListExport;
		}
	});
}

/* Default vars
productListExport contains all the export asins data. It's written by this way just to remember easily which position is attached to whom.
Inserting new products is done by the place of the suppliers id. For example, inserting products to banggoodlist will be productListExport[3].push(var) */
productList = [];
productListExport = [];
AmazonUSlist = []; // 0
AmazonUKlist = []; // 1
AmazonDElist = []; // 2
BanggoodList = []; // 3
AliexpressList =[]; // 4
WalmartList = []; // 5
homeDepotList = []; // 6
AmazonITlist = []; // 7
AmazonFRlist = []; // 8
WayfairUKlist = []; // 9
WayfairCOMlist = []; // 10
costcoCOMlist = []; // 11
overstockCOMlist = []; // 12
chinabrandsCOMlist = []; // 13
costwayCOMlist = []; // 14
DHGateCOMlist = []; // 14
productListExport.push(AmazonUSlist,AmazonUKlist,AmazonDElist,BanggoodList,AliexpressList,WalmartList, homeDepotList, AmazonITlist, AmazonFRlist, WayfairUKlist, WayfairCOMlist, costcoCOMlist, overstockCOMlist, chinabrandsCOMlist, costwayCOMlist, DHGateCOMlist);
var sourceSite;
var uiFixedPanel = 1;

// Get source site and saved products list on page load
getSourceSite();
getProductList();
// Get grabber state - is it hidden? is it visible? and hide / display it by cookie value.
state = readCookie('divState');
if (state=='hidden') {
	createCookie('divState','hidden');
	console.log('set hidden');
	$('.bigContainer').css('display', 'none');
} else if (state=='visible') {
	createCookie('divState','visible');
	console.log('set visible');
	$('.bigContainer').css('display', 'block');
}

// Also changes cookie value on logo click.
$('#grabberDS').click(function(){
	$(".bigContainer").toggle(1000, function(){
		state = readCookie('divState');
		if (state=='hidden') {
			createCookie('divState','visible');
		} else if (state=='visible') {
			createCookie('divState','hidden');
		} else {
			createCookie('divState','visible');
		}
	});
});

// Attach event to export button
$('#exportToDS').click(function(){
	downloadCSV(/*{ filename: sourceSite+" Export.csv" }*/);
});

// Delete button. Make sure to edit the productListExport variable when adding / removing a suppliers list!!
$('#deleteGrab').click(function(){
	productList = [];
	productListExport = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]];
	$('#grabberTable tbody tr').remove();
	chrome.storage.local.clear();
	$('#counter').get(0).innerText="Products: 0";
});

// Extract ASIN by given URL. Check for regexes, matching the source site. return source and prooductID as list ( [source, productId] )
function hrefExtract(href){

	var extra_value = "";

	if (href.toLowerCase().includes('amazon.com')) {
		source = 'amazon_us';
		productId = href.match("(?:[/dp/ /]|$)(B[A-Z0-9]{9})");
		if (productId != null) {
			productId = productId[1];
		}
	} else if (href.toLowerCase().includes('amazon.co.uk')) {
		source = 'amazon_co_uk';
		productId = href.match("(?:[/dp/ /]|$)(B[A-Z0-9]{9})");
		if (productId != null) {
			productId = productId[1];
		}
	} else if (href.toLowerCase().includes('amazon.de')) {
		source = 'amazon_de';
		productId = href.match("(?:[/dp/ /]|$)(B[A-Z0-9]{9})");
		if (productId != null) {
			productId = productId[1];
		}
	} else if (href.toLowerCase().includes('amazon.it')) {
		source = 'amazon_it';
		productId = href.match("(?:[/dp/ /]|$)(B[A-Z0-9]{9})");
		if (productId != null) {
			productId = productId[1];
		}
	} else if (href.toLowerCase().includes('amazon.fr')) {
		source = 'amazon_fr';
		productId = href.match("(?:[/dp/ /]|$)(B[A-Z0-9]{9})");
		if (productId != null) {
			productId = productId[1];
		}
	} else if (href.toLowerCase().includes('walmart')) {
		source = 'walmart';
		productId = href.match("([0-9]{7,10})")[0];
	} else if (href.toLowerCase().includes('aliexpress')) {
		source = 'aliexpress';
		productId = href.match("([0-9]{10,11})")[0];
	} else if (href.toLowerCase().includes('ebay.com')) {
		source = 'ebay_us';
		productId = href.match("([0-9]{12})")[0];
	} else if (href.toLowerCase().includes('ebay.co.uk')) {
		source = 'ebay_co_uk';
		productId = href.match("([0-9]{12})")[0];
	} else if (href.toLowerCase().includes('ebay.de')) {
		source = 'ebay_de';
		productId = href.match("([0-9]{12})")[0];
	} else if (href.toLowerCase().includes('ebay.it')) {
		source = 'ebay_it';
		productId = href.match("([0-9]{12})")[0];
	} else if (href.toLowerCase().includes('ebay.fr')) {
		source = 'ebay_fr';
		productId = href.match("([0-9]{12})")[0];
	} else if (href.toLowerCase().includes('banggood.com')) {
		source = 'banggood';
		productId = href.match("([0-9]{5,8})")[0];
	} else if (href.toLowerCase().includes('homedepot')) {
        source = 'homedepot';
        productId = href.match("([0-9]{9})")[0];
    } else if (href.toLowerCase().includes('wayfair.co.uk')) {
        source = 'wayfair';
        href = href.split('-')
        for (i=0;i<href.length;i++) {
            if (href[i].indexOf('.html') > 0) {
                idDotHtml = href[i]
                productId = idDotHtml.split('.')[0].toUpperCase()
            }
        }
    } else if (href.toLowerCase().includes('wayfair.com')) {
        source = 'wayfair';
        href = href.split('-')
        for (i=0;i<href.length;i++) {
            if (href[i].indexOf('.html') > 0) {
                idDotHtml = href[i]
                productId = idDotHtml.split('.')[0].toUpperCase()
            }
        }
    } else if (href.toLowerCase().includes('.costco.com')) {
        console.log(href);
        source = 'costco COM';
        productId = href.match("([0-9]{9})")[0];

    } else if (href.toLowerCase().includes('.chinabrands.com')) {
        source = 'Chinabrands COM';

        var href_a = href.split("?");
        console.log(href_a);
        if( typeof href_a !== 'undefined' ) {
        	href = href_a[0];
        }

        productId = href.match("([0-9]{4,})-p")[1];

        extra_value = href;

        console.log(productId);
        
    } else if (href.toLowerCase().includes('.costway.com')) {
        source = 'Costway COM';

        var href_a = href.split("/");
        if( typeof href_a !== 'undefined' ) {
        	href = href_a[3];
        }

        productId = href.replace(".html", '');

        extra_value = href;
        
    } else if (href.toLowerCase().includes('.dhgate.com')) {
        source = 'DHGate COM';

        var href_a = href.split("/");
        if( typeof href_a !== 'undefined' ) {
        	href = href_a[5];
        }

        productId = href.match("([0-9]{9})")[0];
        extra_value = href;
        
    } else if (href.toLowerCase().includes('.overstock.com')) {
        source = 'overstock COM';
        productId = href.match("([0-9]{7})")[0];
    } 
	return [source, productId, extra_value];
}

// Attaching events by source site to the Grab button
$('#bulkGrab').click(function(e){
	e.preventDefault();
	var productID;
	if (sourceSite === "Amazon US") {
		try {
			var div = null;
			var links = null;
			var i = 0;
			/* Loop through all predefined divisions in page until it matches one. This is build that way to get only the relevant results, for example in a search results, excluding
			"Suggested pproducts" and "Especially for you" and "Sponsored products" etc. */
			while (div == null && i<10) {
				if (i == 0) {
					div = document.getElementById('rightContainerATF');
				} else if (i == 1) {
					div = document.getElementById('zg');
				} else if (i == 2) {
					div = document.getElementById('widgetContent');
				} else if (i == 3) {
					div = document.getElementById('ys-card');
				} else if (i == 4) {
					div = document.getElementById('search-results');
				} else if (i == 5) {
					div = document.getElementById('ask_lazy_load_div');
				} else if (i == 6) {
					div = document.getElementById('container');
				} else if (i == 7) {
					div = document.getElementById('content-right');
				} else if (i == 8) {
					//div = document.getElementsByClassName('sg-col-inner')[0];
					div = document.getElementsByClassName('s-result-list')[0];
				} else if (i == 9) {
					div = document.getElementsByClassName('stores-page')[0];
				}
				i += 1;
			}
			// When matches a div, get all of the links inside that div.
			links = div.getElementsByTagName('a');
			productID = [];
			// Loop through links and run the hrefExtract for each to get source and product id
			for (i=0;i<links.length;i++) {
				extract = hrefExtract(links[i].href);
				if (extract[1] != null) {
					pId = extract[1];
					// Add found id to list
					productID.push(pId);
				}
			}
			// Remove duplicates from new grabbed products
			productID = [...new Set(productID)];
			// Append found data to export list, and regular product list.
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://www.amazon.com/dp/'+productID[i]
				};
				productObjectExport = {
					'ID': productID[i]
				};

				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[0].push(productObjectExport);
				}				
			}
		} catch(er) {
			console.log(e);
			alert('Can not extract products from this page. Please use the search field.');
		}
		// Goes on for every supplier....
	} else if (sourceSite === "Amazon UK") {
		try {
			var div = null;
			var links = null;
			var i = 0;
			while (div == null && i<10) {
				if (i == 0) {
					div = document.getElementById('rightContainerATF');
				} else if (i == 1) {
					div = document.getElementById('zg');
				} else if (i == 2) {
					div = document.getElementById('widgetContent');
				} else if (i == 3) {
					div = document.getElementById('ys-card');
				} else if (i == 4) {
					div = document.getElementById('search-results');
				} else if (i == 5) {
					div = document.getElementById('ask_lazy_load_div');
				} else if (i == 6) {
					div = document.getElementById('container');
				} else if (i == 7) {
					div = document.getElementById('content-right');
				} else if (i == 8) {
					//div = document.getElementsByClassName('sg-col-inner')[0];
                    div = document.getElementsByClassName('s-result-list')[0];
				} else if (i == 9) {
					div = document.getElementsByClassName('stores-page')[0];
				}
				i += 1;
			}
			links = div.getElementsByTagName('a');
			productID = [];
			for (i=0;i<links.length;i++) {
				extract = hrefExtract(links[i].href);
				if (extract[1] != null) {
					pId = extract[1];
					productID.push(pId);
					// console.log(links[i]);
				}
			}
			productID = [...new Set(productID)];
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://www.amazon.co.uk/dp/'+productID[i]
				};
				productObjectExport = {
					'ID': productID[i]
				};

				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[1].push(productObjectExport);
				}
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}
	} else if (sourceSite === "Amazon DE") {
		try {
			var div = null;
			var links = null;
			var i = 0;
			while (div == null && i<10) {
				if (i == 0) {
					div = document.getElementById('rightContainerATF');
				} else if (i == 1) {
					div = document.getElementById('zg');
				} else if (i == 2) {
					div = document.getElementById('widgetContent');
				} else if (i == 3) {
					div = document.getElementById('ys-card');
				} else if (i == 4) {
					div = document.getElementById('search-results');
				} else if (i == 5) {
					div = document.getElementById('ask_lazy_load_div');
				} else if (i == 6) {
					div = document.getElementById('container');
				} else if (i == 7) {
					div = document.getElementById('content-right');
				} else if (i == 8) {
					//div = document.getElementsByClassName('sg-col-inner')[0];
                    div = document.getElementsByClassName('s-result-list')[0];
				} else if (i == 9) {
					div = document.getElementsByClassName('stores-page')[0];
				}
				i += 1;
			}
			links = div.getElementsByTagName('a');
			productID = [];
			for (i=0;i<links.length;i++) {
				extract = hrefExtract(links[i].href);
				if (extract[1] != null) {
					pId = extract[1];
					productID.push(pId);
					// console.log(links[i]);
				}
			}
			productID = [...new Set(productID)];
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://www.amazon.de/dp/'+productID[i]
				};
				productObjectExport = {
					'ID': productID[i]
				};
				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[2].push(productObjectExport);
				}
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}
	} else if (sourceSite === "Amazon IT") {
		try {
			var div = null;
			var links = null;
			var i = 0;
			while (div == null && i<10) {
				if (i == 0) {
					div = document.getElementById('rightContainerATF');
				} else if (i == 1) {
					div = document.getElementById('zg');
				} else if (i == 2) {
					div = document.getElementById('widgetContent');
				} else if (i == 3) {
					div = document.getElementById('ys-card');
				} else if (i == 4) {
					div = document.getElementById('search-results');
				} else if (i == 5) {
					div = document.getElementById('ask_lazy_load_div');
				} else if (i == 6) {
					div = document.getElementById('container');
				} else if (i == 7) {
					div = document.getElementById('content-right');
				} else if (i == 8) {
					//div = document.getElementsByClassName('sg-col-inner')[0];
                    div = document.getElementsByClassName('s-result-list')[0];
				} else if (i == 9) {
					div = document.getElementsByClassName('stores-page')[0];
				}
				i += 1;
			}
			links = div.getElementsByTagName('a');
			productID = [];
			for (i=0;i<links.length;i++) {
				extract = hrefExtract(links[i].href);
				if (extract[1] != null) {
					pId = extract[1];
					productID.push(pId);
					// console.log(links[i]);
				}
			}
			productID = [...new Set(productID)];
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://www.amazon.it/dp/'+productID[i]
				};
				productObjectExport = {
					'ID': productID[i]
				};
				
				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[7].push(productObjectExport);
				}
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}
	} else if (sourceSite === "Amazon FR") {
		try {
			var div = null;
			var links = null;
			var i = 0;
			while (div == null && i<10) {
				if (i == 0) {
					div = document.getElementById('rightContainerATF');
				} else if (i == 1) {
					div = document.getElementById('zg');
				} else if (i == 2) {
					div = document.getElementById('widgetContent');
				} else if (i == 3) {
					div = document.getElementById('ys-card');
				} else if (i == 4) {
					div = document.getElementById('search-results');
				} else if (i == 5) {
					div = document.getElementById('ask_lazy_load_div');
				} else if (i == 6) {
					div = document.getElementById('container');
				} else if (i == 7) {
					div = document.getElementById('content-right');
				} else if (i == 8) {
					//div = document.getElementsByClassName('sg-col-inner')[0];
                    div = document.getElementsByClassName('s-result-list')[0];
				} else if (i == 9) {
					div = document.getElementsByClassName('stores-page')[0];
				}
				i += 1;
			}
			links = div.getElementsByTagName('a');
			productID = [];
			for (i=0;i<links.length;i++) {
				extract = hrefExtract(links[i].href);
				if (extract[1] != null) {
					pId = extract[1];
					productID.push(pId);
					// console.log(links[i]);
				}
			}
			productID = [...new Set(productID)];
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://www.amazon.fr/dp/'+productID[i]
				};
				productObjectExport = {
					'ID': productID[i]
				};
				
				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[8].push(productObjectExport);
				}
				
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}
	} else if (sourceSite === "Banggood") {
		try {
			var div = null;
			var links = null;
			var i = 0;
			while (div == null && i<7) {
				if (i == 0) {
					div = document.getElementsByClassName('good_main')[0];
				} else if (i == 1) {
					div = document.getElementsByClassName('wrap_right')[0];
				} else if (i == 2) {
					div = document.getElementsByClassName('newarrivals_list')[0];
				} else if (i == 3) {
					div = document.getElementById('brandsrecommend');
				} else if (i == 4) {
					div = document.getElementById('cc');
				} else if (i == 5) {
					div = document.getElementById('jspromsgwrap');
				} else if (i == 6) {
					div = document.getElementsByClassName('category_new_list')[0];
				}
				i += 1;
			}
			if (document.URL.indexOf('Flashdeals') > -1) {
				divs = document.getElementsByClassName('goodlist_1');
				links = []
				for (i=0;i<divs.length;i++) {
					links = links.concat(Array.from(divs[i].getElementsByTagName('a')));
				}
			} else {
				links = div.getElementsByTagName('a');
			}
			productID = [];
			for (i=0;i<links.length;i++) {
				try {
					extract = hrefExtract(links[i].href);
				} catch (err) {
					continue;
				}
				if (extract[1] != null) {
					pId = extract[1];
					productID.push(pId);
					// console.log(links[i]);
				}
			}
			productID = [...new Set(productID)];
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://www.banggood.com/-p-'+productID[i]+'.html'
				};
				productObjectExport = {
					'ID': productID[i]
				};
				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[3].push(productObjectExport);
				}
				
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}
	} else if (sourceSite === "Banggood US") {
		try {
			var div = null;
			var links = null;
			var i = 0;
			while (div == null && i<6) {
				if (i == 0) {
					div = document.getElementsByClassName('main_content')[0];
				} else if (i == 1) {
					div = document.getElementsByClassName('productlist')[0];
				} else if (i == 2) {
					div = document.getElementsByClassName('pro_wrap box')[0];
				} else if (i == 3) {
					div = document.getElementById('recommendations');
				} else if (i == 4) {
					div = document.getElementById('bd-container');
                } else if (i == 5) {
					div = document.getElementsByClassName('good_box_min')[0];
				}
				i += 1;
			}
			links = div.getElementsByTagName('a');
			productID = [];
			for (i=0;i<links.length;i++) {
				try {
					extract = hrefExtract(links[i].href);
				} catch (err) {
					continue;
				}
				if (extract[1] != null) {
					pId = extract[1];
					productID.push(pId);
					// console.log(links[i]);
				}
			}
			productID = [...new Set(productID)];
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://us.banggood.com/-p-'+productID[i]+'.html'
				};
				productObjectExport = {
					'ID': productID[i]
				};
				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[3].push(productObjectExport);
				}
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}
	} else if (sourceSite === "Aliexpress") {
		try {
			if (uiFixedPanel == 1) {
				try {
					document.getElementsByClassName('ui-fixed-panel')[0].remove();
					uiFixedPanel = 0;
				} catch (e) {
					console.log('UI fixed panel not found');
				}
			}
			var div = null;
			var links = null;
			var i = 0;
			while ((div == null || div == undefined) && i<10) {
				if (i == 0) {
					div = document.getElementById('gallery-item');
				} else if (i == 1) {
					div = document.getElementsByClassName('ui-box contact-seller-email')[0];
				} else if (i == 2) {
					div = document.getElementById('bd-container');
				} else if (i == 3) {
					div = document.getElementById('hs-list-items');
				} else if (i == 4) {
					div = document.getElementById('related-recommend-entry');
				} else if (i == 5) {
					div = document.getElementsByClassName('deals-container')[0];
				} else if (i == 6) {
					div = document.getElementsByClassName('horizontal wrap prime-list')[0];
				} else if (i == 7) {
					div = document.getElementsByClassName('ui-box-body')[0];
				} else if (i == 8) {
					div = document.getElementsByClassName('product-container')[0];
				} else if (i == 9) {
					div = document.getElementById('list-items');
				}
				i += 1;
			}
			links = div.getElementsByTagName('a');
			productID = [];
			for (i=0;i<links.length;i++) {
				try {
					extract = hrefExtract(links[i].href);
				} catch (err) {
					continue
				}
				if (extract[1] != null) {
					pId = extract[1];
					productID.push(pId);
					// console.log(links[i]);
				}
			}
			productID = [...new Set(productID)];
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://www.aliexpress.com/item/-/'+productID[i]+'.html'
				};
				productObjectExport = {
					'ID': productID[i]
				};
				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[4].push(productObjectExport);
				}
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}
	} else if (sourceSite === "Walmart") {
		try {
			var div = null;
			var links = null;
			var i = 0;
			var use_url = false;
			while (div == null && i<5) {
				if (i == 0) {
					div = document.getElementsByClassName('CategoryApp-main')[0];
				} else if (i == 1) {
					div = document.getElementById('mainSearchContent');
				} else if (i == 2) {
					div = document.getElementById('customer-reviews');
					use_url = true
				} else if (i == 3) {
					div = document.getElementsByClassName('Grid-col u-size-1-2-m')[0];
				} else if (i == 4) {
					div = document.getElementById('related-recommend-entry');
				}
				i += 1;
			}
			if (links == null) {
				links = div.getElementsByTagName('a');
			}
			productID = [];
			if (use_url == false) {
				for (i=0;i<links.length;i++) {
					try {
						console.log('1')
						extract = hrefExtract(links[i].href);
					} catch (e) {
						continue
					}
					if (extract[1] != null) {
						pId = extract[1];
						productID.push(pId);
					}
				}
				productID = [...new Set(productID)];
			} else {
				productID = [hrefExtract(document.URL)[1]]
			}
			for (i=0;i<productID.length;i++) {
				productObject = {
					'ID': productID[i],
					'URL': 'https://www.walmart.com/ip/'+productID[i]
				};
				productObjectExport = {
					'ID': productID[i]
				};
				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[5].push(productObjectExport);
				}
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}

	} else if (sourceSite === "HomeDepot") {
        try {
            var div = null;
            var links = null;
            var i = 0;
            while ((div == null || div == undefined) && i<2) {
                if (i == 0) {
                    div = document.getElementById('products');
                } else if (i == 1) {
                    div = document.getElementsByClassName('ProductListGroup')[0];
                }
                i += 1;
            }
            links = div.getElementsByTagName('a');
            productID = [];
            for (i=0;i<links.length;i++) {
                try {
                    extract = hrefExtract(links[i].href);
                } catch (err) {
                    continue
                }
                if (extract[1] != null) {
                    pId = extract[1];
                    productID.push(pId);
                }
            }
            productID = [...new Set(productID)];
            for (i=0;i<productID.length;i++) {
                productObject = {
                    'ID': productID[i],
                    'URL': 'https://www.homedepot.com/p/'+productID[i]
                };
                productObjectExport = {
                    'ID': productID[i]
                };
                let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
                	productListExport[6].push(productObjectExport);
				}
            }
        } catch(err) {
            console.log(err);
            alert('Can not extract products from this page. Please use the search field.');
        }
    } else if (sourceSite === "Wayfair UK") {
        try {
            var div = null;
            var links = null;
            var i = 0;
            while ((div == null || div == undefined) && i<3) {
                if (i == 0) {
                    div = document.getElementById('sbprodgrid');
                } else if (i == 1) {
                    div = document.getElementsByClassName('ShopTheLookRoom-feedWrap')[0];
                } else if (i == 2) {
                    div = document.getElementsByClassName('CompareItem--current')[0];
                }
                i += 1;
            }
            links = div.getElementsByTagName('a');
            productID = [];
            for (i=0;i<links.length;i++) {
                try {
                    extract = hrefExtract(links[i].href);
                } catch (err) {
                    continue
                }
                if (extract[1] != null) {
                    pId = extract[1];
                    productID.push(pId);
                }
            }
            productID = [...new Set(productID)];
            for (i=0;i<productID.length;i++) {
                productObject = {
                    'ID': productID[i],
                    'URL': 'https://www.wayfair.co.uk/keyword.php?keyword='+productID[i]
                };
                productObjectExport = {
                    'ID': productID[i]
                };
                let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
                	productListExport[9].push(productObjectExport);
				}
            }
        } catch(err) {
            console.log(err);
            alert('Can not extract products from this page. Please use the search field.');
        }
    } else if (sourceSite === "Wayfair COM") {
        try {
            var div = null;
            var links = null;
            var i = 0;
            while ((div == null || div == undefined) && i<3) {
                if (i == 0) {
                    div = document.getElementById('sbprodgrid');
                } else if (i == 1) {
                    div = document.getElementsByClassName('ShopTheLookRoom-feedWrap')[0];
                } else if (i == 2) {
                    div = document.getElementsByClassName('CompareItem--current')[0];
                }
                i += 1;
            }
            links = div.getElementsByTagName('a');
            productID = [];
            for (i=0;i<links.length;i++) {
                try {
                    extract = hrefExtract(links[i].href);
                } catch (err) {
                    continue
                }
                if (extract[1] != null) {
                    pId = extract[1];
                    productID.push(pId);
                }
            }
            productID = [...new Set(productID)];
            for (i=0;i<productID.length;i++) {
                productObject = {
                    'ID': productID[i],
                    'URL': 'https://www.wayfair.com/keyword.php?keyword='+productID[i]
                };
                productObjectExport = {
                    'ID': productID[i]
                };
                let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
                	productListExport[10].push(productObjectExport);
				}
            }
        } catch(err) {
            console.log(err);
            alert('Can not extract products from this page. Please use the search field.');
        }
    } else if (sourceSite === "Chinabrands COM") {
        try {
            var div = null;
            var inner_div = null;
            var links = null;
            var i = 0;

            //pro-list
            links = $('#pro-list').find('.goods-title a');
            console.log('new site');
            productID = [];
            var urls = [];
            for (i=0;i<links.length;i++) {
                try {
                    extract = hrefExtract(links[i].href);
                } catch (err) {
                    continue
                }
                if (extract[1] != null) {
                    pId = extract[1];
                    urls.push(extract[2]);
                    productID.push(pId);
                }
            }

            productID = [...new Set(productID)];
            for (i=0;i<productID.length;i++) {
                productObject = {
                    'ID': productID[i],
                    'URL': urls[i]
                };
                productObjectExport = {
                    'ID': productID[i]
                };
                let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
                	productListExport[13].push(productObjectExport);
				}
            }

        } catch(err) {
            console.log(err);
            alert('Can not extract products from this page. Please use the search field.');
        }
    }else if (sourceSite === "Costway COM") {
        try {
            var div = null;
            var inner_div = null;
            var links = null;
            var i = 0;

            //pro-list
            links = $('#pro_box').find('ul li a');
            console.log('links');
            console.log(links);

            if(links.length <= 0){
            	throw "Can not extract products from this page. Please use the search field.";
            }
            productID = [];
            var urls = [];
            for (i=0;i<links.length;i++) {
                try {
                    extract = hrefExtract(links[i].href);
                } catch (err) {
                    continue
                }
                if (extract[1] != null) {
                    pId = extract[1];
                    urls.push(extract[2]);
                    productID.push(pId);
                }
            }

            productID = [...new Set(productID)];

            for (i=0;i<productID.length;i++) {
                productObject = {
                    'ID': productID[i],
                    'URL': urls[i]
                };
                productObjectExport = {
                    'ID': productID[i]
                };
                let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
                	productListExport[15].push(productObjectExport);
				}
            }

        } catch(err) {
            console.log(err);
            alert('Can not extract products from this page. Please use the search field.');
        }
    }else if (sourceSite === "DHGate COM") {
        try {
            var div = null;
            var inner_div = null;
            var links = null;
            var i = 0;

            //pro-list
            links = $('#proGallery').find('.gwrap .gitem .pro-title a');
            
            if(links.length <= 0){
            	throw "Can not extract products from this page. Please use the search field.";
            }

            productID = [];
            var urls = [];
            for (i=0;i<links.length;i++) {
                try {
                    extract = hrefExtract(links[i].href);
                } catch (err) {
                    continue
                }
                if (extract[1] != null) {
                    pId = extract[1];
                    urls.push(extract[2]);
                    productID.push(pId);
                }
            }

            productID = [...new Set(productID)];

            for (i=0;i<productID.length;i++) {
                productObject = {
                    'ID': productID[i],
                    'URL': urls[i]
                };
                productObjectExport = {
                    'ID': productID[i]
                };
                let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
                	productListExport[15].push(productObjectExport);
				}
            }

        } catch(err) {
            console.log(err);
            alert('Can not extract products from this page. Please use the search field.');
        }
    } else if (sourceSite === "Costco COM") {
        
		try {
            
			var div = null;
			var links = null;
			var detailPage = false;
			var i = 0;
			while ((div == null || div == undefined) && i<2) {
				if (i == 0) {
					div = document.getElementById('product-details');
                    link = document.URL;
                    detailPage =true;
				} else if (i == 1) {
					div = document.getElementsByClassName('product-list grid')[0];
                    detailPage =false;
				} 
				i += 1;
			}
			links = div.getElementsByTagName('a');
			productID = [];
            if(detailPage == true){
                var pId = $(document).find('p.item-number').text();
                if(pId != undefined){
                    console.log(pId);
                    if(pId.indexOf("Item") !== -1){
                        pId = pId.split(' ');
                        console.log(pId);
                        if(pId[1] != undefined){
                            productID.push(pId[1]);
                        }
                    }
                    else{
                        productID.push(pId);
                    }
                    
                }
                productID = [...new Set(productID)];
                productObject = {
                    'ID': productID[0],
                    'URL': link
                };
                productObjectExport = {
                    'ID': productID[0]
                };
                let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
                	productListExport[11].push(productObjectExport);
				}
            }
            else{
                var currentPage = 'costcoDetail';
                $(document).find('#content-spinner-small').show();
                for (i=0;i<links.length;i++) {
                    var promise = $.ajax({
                        url: links[i].href,
                        type: 'GET',
                        //async: false,
                        complete: function(qXHR,textStatus) {
                            if (textStatus === 'success') {
                                var data = qXHR.responseText;
                                var pId = $(data).find('p.item-number').text();
                                if(pId != undefined){
                                    if(pId.indexOf("Item") !== -1){
                                        pId = pId.split(' ');
                                        console.log(pId);
                                        if(pId[1] != undefined){
                                            productID.push(pId[1]);
                                        }
                                    }
                                    else{
                                        productID.push(pId);
                                    }
                                }
                            } 
                        }

                    });
                }
                promise.then(function(){
                    setTimeout(function(){  
                        productID = [...new Set(productID)];
                        console.log(productID); 
                        for (i=0;i<productID.length;i++) {
                            productObject = {
                                'ID': productID[i],
                                'URL': links[i].href
                            };
                            productObjectExport = {
                                'ID': productID[i]
                            };
                            let obj = productList.find(o => o.ID === productID[i]);
							if(typeof obj === 'undefined'){
								productList.push(productObject);
                            	productListExport[11].push(productObjectExport);
							}
                            
                        }
                        $(document).find('#content-spinner-small').hide();
                        
                        var table = document.getElementById("grabberTable").getElementsByTagName('tbody')[0];
                        // Remove empty tr elements
                        $('#grabberTable tbody tr').remove();
                        // Insert found data to table, as links.
                        for (i=0;i<productList.length;i++){
                            var row = table.insertRow(-1);
                            // var cell1 = row.insertCell(0).innerHTML = '<h4>'+productList[i].ID+'</h4>';
                            var cell1 = row.insertCell(0).innerHTML = '<a href='+productList[i].URL+' target="_blank">'+productList[i].ID+'</a>';
                        }
                        // Change the counter text ("Total products: XXX") to products length.
                        $('#counter').get(0).innerText="Products: "+productList.length;
                        // Store data in local storage.
                        chrome.storage.local.set({
                            'productList': productList,
                            'productListExport': productListExport,
                        });
                        console.log(productListExport);
                    }, 5000);

                });
                
            }
            
		} catch(err) {
            console.log(err);
			alert('Can not extract products from this page. Please use the search field.');
		}
	} else if (sourceSite === "overstock COM") {
		try {
			var div = null;
			var links = null;
			var detailPage = false;
			var i = 0;
			while ((div == null || div == undefined) && i<2) {
				if (i == 0) {
					div = document.getElementsByClassName('add-to-cart-container')[0];
                    link = document.URL;
                    detailPage =true;
				} else if (i == 1) {
					div = document.getElementById('product-container');
                    detailPage =false;
                    links = div.getElementsByTagName('a');
				} 
				i += 1;
			}
            productID = [];
            if(detailPage == true){
                extract = hrefExtract(link);
                if (extract[1] != null) {
                    pId = extract[1];
                    productID.push(pId);
                }
            }
            else{
                for (i=0;i<links.length;i++) {
                    try {
                        extract = hrefExtract(links[i].href);
                    } catch (err) {
                        continue
                    }
                    if (extract[1] != null) {
                        pId = extract[1];
                        productID.push(pId);
                    }
                }
            }
			productID = [...new Set(productID)];
			for (i=0;i<productID.length;i++) {
				
                if(detailPage == true){
                   productObject = {
					'ID': productID[i],
					'URL': link
                    }; 
                }
                else{
                    productObject = {
					'ID': productID[i],
					'URL': links[i].href
                    };
                }
				productObjectExport = {
					'ID': productID[i]
				};
				let obj = productList.find(o => o.ID === productID[i]);
				if(typeof obj === 'undefined'){
					productList.push(productObject);
					productListExport[12].push(productObjectExport);
				}
			}
		} catch(err) {
			alert('Can not extract products from this page. Please use the search field.');
		}
	} 
    // Get the results table
    if(currentPage != 'costcoDetail'){
        var table = document.getElementById("grabberTable").getElementsByTagName('tbody')[0];
        // Remove empty tr elements
        $('#grabberTable tbody tr').remove();
        // Insert found data to table, as links.
        for (i=0;i<productList.length;i++){
            var row = table.insertRow(-1);
            // var cell1 = row.insertCell(0).innerHTML = '<h4>'+productList[i].ID+'</h4>';
            var cell1 = row.insertCell(0).innerHTML = '<a href='+productList[i].URL+' target="_blank">'+productList[i].ID+'</a>';
        }
        // Change the counter text ("Total products: XXX") to products length.
        $('#counter').get(0).innerText="Products: "+productList.length;
        // Store data in local storage.
        chrome.storage.local.set({
            'productList': productList,
            'productListExport': productListExport,
        });
        console.log(productListExport);
    }
});
