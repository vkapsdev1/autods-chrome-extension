var home = "https://expressfy.com/chrome/";
//var home = "http://localhost:8080/Shopify/";
var base = home+"api/";
var token;
var apiKey = "";
var orders = [];
var orderMapping;

chrome.storage.sync.get(["token","notFirst"], function(rsp) {
	if (rsp && rsp.token && rsp.token.length > 0) {
		token = rsp.token;
	} else {
		try {
			$.ajax({
				url : home + "getToken",
				type : "POST",
				success : function(result) {
					
					if(result && typeof (result = JSON.parse(result)) === "object" && result.token ){
						token = result.token;
						chrome.storage.sync.set({
							'token' : result.token
						});
				        chrome.tabs.create({ url: "http://expressfy.com/support/"}); //("http://expressfy.com/start-here" + (result.store?("#" + result.store):""))
				    }else{
				    	if(rsp && rsp.notFirst){}else{
				    		chrome.runtime.openOptionsPage();
				    	}
				    }
				},
				error : function(jqXHR, textStatus, errorThrown) {
					if(rsp && rsp.notFirst){}else{
						chrome.runtime.openOptionsPage();
					}
				}
			});
		} catch (e) {
			console.error("Caught:", e);
			if(rsp && rsp.notFirst){}else{
				chrome.runtime.openOptionsPage();
			}
		}
	}
	chrome.storage.sync.set({
		'notFirst' : true
	});
});

function addProduct(params, resp, map, link) {
	//console.log("addProduct", params);
	var option1;
	if(params.option1){
		//console.log("setting option1 to:", params.option1);
		option1 = params.option1;
		delete params.option1;
	}
	var req = {
		token : token,
		link : link,
		product : JSON.stringify(params)//,
		//name: params.product.title,
	};
	if(option1){
		req.option1 = option1;
	}

	$.ajax({
		url : base + "add",
		type : "POST",
		async: false,
		data : JSON.stringify(req),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		success : function(result) {
			var ok = result.rspCode - 200 < 100;
			resp({
				status : ok
			});
			if (ok) {
				if (map) {
					var product = JSON.parse(result.response);
					$.each(product.product.images, function(index) {
						var id = this.id;
						$.each(map, function() {
							if (this.index == index) {
								this.imgId = id;
								this.variantsIds = [];
							}
						});
					});
					$.each(product.product.variants, function() {
						var id = this.id;
						var title = this.title;
						var o1 = this.option1, o2 = this.option2, o3 = this.option3;
						$.each(map, function() {
							if (this.variantsIds && (this.val === o1 || this.val == o2 || this.val == o3)) {
								this.variantsIds.push(id);
							}
						});
					});
					// console.log("MAP",map);
					$.each(map, function() {
						if (this.imgId && this.variantsIds) {
							var url = product.product.id + "/images/" + this.imgId + ".json";
							var image = {
								image : {
									id : this.imgId,
									variant_ids : this.variantsIds
								}
							};
							modifyImage(url, image);
						}
					});
				}
			}else{
				console.error("addProduct() error:",result);
			}
		}, error : function(jqXHR, textStatus, errorThrown) {
			console.error("addProduct() error: ", jqXHR, textStatus, errorThrown);
		}
	});

}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.command == "addProduct") {
		addProduct(request.data, sendResponse, request.map, request.link);
	} else if (request.command == "setShopifyToken" && request.token) {
		//console.log("@setShopifyToken", request);
		token = request.token;
		chrome.storage.sync.set({
			'token' : token
		});
		sendResponse({rsp:"wasSet"});
		return false;
	}else if(request.command == "state"){
		$.ajax({
			url : home + "checkState",
			type : "POST",
			data : {
				token : token
			},
			success : function(result) {
				//console.log("@state", result);
				if(result && typeof (result = JSON.parse(result))  === 'object' && result.uninstalled !== undefined && result.disabled !== undefined){
					sendResponse(result);
				}else{
					sendResponse({uninstalled:true});
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				sendResponse(undefined);
			}
		});
		return true;
	} else if(request.command == "getShopifyToken"){
		$.ajax({
			url : home + "getToken",
			type : "POST",
			success : function(result) {
				if(result && typeof (result = JSON.parse(result)) === "object" && result.token ){
					token = result.token;
					chrome.storage.sync.set({
						'token' : result.token
					});
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				//chrome.runtime.openOptionsPage();
			}
		});
		return false;
	} else if(request.command == "getOrders"){
		loadOrders(function(){
			sendResponse(orders);
		});
	}  else if(request.command == "tokenReq"){
		sendResponse(token);
	} else if(request.command == "clearOrder"){
		orderMapping = undefined;
		sendResponse({msg:"The Order Was Cleared"});
	}  else if(request.command == "order"){
		chrome.tabs.create({url:request.order.link}, function(tab){
			orderMapping = {tabId:tab.id, order:request.order};
			//console.log("order tab was opened: ",tab);
		});
		return false;
	} else if(request.command == "getOrder"){
		//console.log("@getOrder",sender)
		if(orderMapping && sender && sender.tab && orderMapping.tabId == sender.tab.id){
			//console.log("The condition was meet");
			sendResponse(orderMapping.order);
		}else{
			sendResponse();
		}
	} else if(request.command == "getAddresses"){
		checkState(function(state){
			var addresses = [];
			if(state && state.ordersState){
				loadOrders(function(){
					//console.log("the orders were loaded", orders);
					$.each(orders, function(key, value){
						addresses.push(value.shipping_address);
					});
					sendResponse(addresses);
				});
			}else{
				//console.log("@getAddresses, 'state' is inactive", state);
				sendResponse(addresses);
			}
		});
	}  else {
		console.error("Unknown request");
	}
	return true;
});

function modifyImage(url, params) {
	$.ajax({
		url : base + "images",
		type : "POST",
		async: false,
		data : JSON.stringify({
			token : token,
			url : url,
			product : JSON.stringify(params)
		}),
		contentType : "application/json; charset=utf-8",
		dataType : "json"
	});
}


chrome.browserAction.onClicked.addListener(function(tab) { 
	chrome.runtime.openOptionsPage();
});

function loadOrders(callBack){
	var params = "token="+token;
	if(orders && orders.length > 0){
		orders.sort(function(a,b){
			return b.id - a.id;
		});
		params+="&last="+orders[0].id;
	}
	//console.log("getting orders, params: ", params);
	$.get(base+"getOrders?"+params,function(data){
		if(data && (data = JSON.parse(data)) instanceof Array){
			//console.log("new orders", data);
			$.each(data, function(k,v){
				var o = orders.filter(function(oo){
					return v.id == oo.id;
				});
				//console.log("filtered",o);
				if(o.length == 0){
					orders.push(v);
				}else{
					console.error("order", v, "already exists")
				}
			});
		}else{
			console.error("no orders", data);
		}
		callBack();
	}).fail(function() {
		callBack();
	});
}

function checkState(callBack){
	$.post(home + "checkState", {token : token}, function(result){
		//console.log("result", result);
		if(result && typeof (result = JSON.parse(result))  === 'object' && result.uninstalled !== undefined && result.disabled !== undefined){
			callBack(result);
		}else{
			callBack({uninstalled:true});
		}
	}).fail(function(){
		callback(undefined);
	});
}
