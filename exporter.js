// Create the HTML table & logo picture that are injected to document.
var imagesPath = chrome.extension.getURL('images'); 
table = $('<div class="bigContainer"> <div class="aboveTable"> <div id="aboveHeader">AutoDS Exporter</div> </div> <div class="tableContainer"> <div style="display:none;" id="content-spinner-small"><img src="'+imagesPath+'/loading.gif"></div> <table id="grabberTable" class="panel-body"> <!-- <thead> <tr> <th>Product ID</th> <th>Title</th> <th>Price</th> <th>Ranking</th> <th>Stars</th> </tr> </thead> --> <tbody> <tr></tr> </tbody> </table> </div> <div class="belowTable"> <div id="counter" class="counter">Products: 0</div> <div> <button id="bulkGrab" class="styleButtons">Extract</button> <button id="deleteGrab" class="styleButtons">Delete</button> </div> <div> <button id="exportToDS" class="styleButtons">Export as CSV</button> <!-- <button id="exportToDS" class="styleButtons" onclick="downloadCSV();">Multi Extractor</button> --> </div> </div> </div>');
grabber = $('<div> <img id="grabberDS" src="https://i.imgur.com/5DSL9ji.png"></img> </div>');
$('body').append(grabber);
$('body').append(table);
function clickChildvariations(){
    var obj1 = jQuery.Deferred();
    var counter = 0;
    var variationDiv = $('.listing_list_item').find("tr td:nth-child(2) p:nth-child(2) a");
    var backCounter = $(variationDiv).length;
    var backCountersecondLast = 0;
    if($(variationDiv).length > 2){
        backCountersecondLast = $(variationDiv).length - 2;
    }
    variationDiv.each(function (index, value) {
        setTimeout(function(){
            value.click();
            counter++;
            backCounter--;
            console.log(counter);
            if(counter == 3){
                setTimeout(function(){
                    obj1.resolve("clickChildvariations");
                },2000);
            }
        },parseInt(index) + parseInt(backCounter) - parseInt(backCountersecondLast)+'000');
        
    })
    return obj1.promise(); 
}
function shopmaster(){
    var rivalsProductListTemp = [];
    var obj = jQuery.Deferred();
    var counter = 0;
    $.when( clickChildvariations() ).then(
        function( status1 ) {
        table = $('table.tr_hover tbody');   
        for (i=0;i<table.length-1;i++) {
            columns = table.children().eq(i).children();
            sourceHrefDiv = columns[1].children[0].children[0].children[0].children[3].children[0];
            ebayHref = columns[1].children[0].children[0].children[0].children[2].children[1].children[0].getAttribute('href');
            extractEbayList = hrefExtract(ebayHref);
            ebayItemId = extractEbayList[1];
            ebaySite = extractEbayList[0];
            if(sourceHrefDiv != undefined){
                sourceHref = $(sourceHrefDiv).find('a').attr('href');
                extractSourceList = hrefExtract(sourceHref);
                sourceId = extractSourceList[1];
                sourceProductSite = extractSourceList[0];
                variationId = '';
            }
            else{
                sourceId = '';
                sourceProductSite = '';  
            }
            productObject = {
                'Product Type': 'Parent',
                'Item ID': ebayItemId,
                'Source ID': sourceId,
                'Source Site': sourceProductSite,
                'To Ebay': ebaySite,
                'Var SKU': ''
            };
            rivalsProductListTemp.push(productObject);
            var varation_info = $(columns).find('.varation_info');
            var childAttr = [];
            if(varation_info != undefined){
                $(varation_info).each(function(){
                    childAttr.push( $(this).find('td:nth-child(3)').text() );
                });
            }
            if(sourceHrefDiv != undefined){
                chrome.runtime.sendMessage({
                    task: "GETRequest",
                    url: sourceHref,
                    parent: ebayItemId,
                    childAttr: childAttr
                }, function(response) { 
                    if(typeof response !== 'undefined' && response != null){
                        $(rivalsProductListTemp).each(function(index,value){ ;
                            if(value['Item ID'] == response[0].parent){
                                var productObjectTemp = [];
                                productObjectTemp = JSON.parse(JSON.stringify(value));
                                rivalsProductList.push(productObjectTemp);
                                rivalsProductListExport[8].push(value);
                                $(response[0].child).each(function(index1, value1){
                                    productObjectTemp["Var SKU"] = value1;
                                    productObjectTemp["Product Type"] = 'Child';
                                    rivalsProductListExport[8].push(productObjectTemp); 
                                });
                            }
                        });
                    } 
                    counter++;
                    console.log(counter);
                    if(counter == table.length-1){
                        obj.resolve("shopmaster");
                    }
                }); 
            }
            else{
                counter++;
                productObject = {
                    'Product Type': 'Parent',
                    'Item ID': ebayItemId,
                    'Source ID': sourceId,
                    'Source Site': sourceProductSite,
                    'To Ebay': ebaySite,
                    'Var SKU': ''
                };
                rivalsProductList.push(productObject);
                rivalsProductListExport[8].push(productObject);  
                if(counter == table.length-1){
                    obj.resolve("shopmaster");
                }
            }
        }
        }
    );
    return obj.promise();
    
}
function writeGrabberTable(){
    // Get the results table
    var table = document.getElementById("grabberTable").getElementsByTagName('tbody')[0];
    // Insert found data to table.
    var start = 0;
    if($(table).find('tr').length > 0){
        start = $(table).find('tr').length;
    }
    for (i=start;i<rivalsProductList.length;i++){
        var row = table.insertRow(-1);
        var cell1 = row.insertCell(0).innerHTML = '<p>'+rivalsProductList[i]['Item ID']+'<p>';
    }
    // Change the counter text ("Total products: XXX") to products length.
    $('#counter').get(0).innerText="Products: "+rivalsProductList.length;
    // Store data in local storage.
    chrome.storage.local.set({
        'rivalsProductList': rivalsProductList,
        'rivalsProductListExport': rivalsProductListExport,
    });
    // console.log(productList);
    console.log(rivalsProductListExport);
}



// Create, read and delete cookie functions
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	} else {
		expires = "";
	}
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

// Download CSV function
function convertArrayOfObjectsToCSV(args) {  
	var result, ctr, keys, columnDelimiter, lineDelimiter, data;

	data = args.data || null;
	if (data == null || !data.length) {
		return null;
	}

	columnDelimiter = args.columnDelimiter || ',';
	lineDelimiter = args.lineDelimiter || '\n';

	keys = Object.keys(data[0]);

	result = '';
	result += keys[0]+',';
	result += keys.join(columnDelimiter);
	result += lineDelimiter;
	// console.log(result);
	data.forEach(function(item) {
		ctr = 0;
		keys.forEach(function(key) {
			if (ctr > 0) result += columnDelimiter;

			result += item[key];
			ctr++;
		});
		result += lineDelimiter;
	});

	return result;
}

// Download CSV function
function downloadCSV(args) {  
	var data, filename, link;

	for (i=0;i<rivalsProductListExport.length;i++){
		if (rivalsProductListExport[i].length > 0){
			var csv = convertArrayOfObjectsToCSV({
				data: rivalsProductListExport[i]
			});
			// This is exporting the file by rival name. Supplier "i" number is by their position in suppliers list (var productListExport)
			if (csv == null) return;
			// filename = args.filename || 'export.csv';
			if (i == 0) {
				filename = 'DSM Export.csv';
			} else if (i == 1) {
				filename = 'Easync Export.csv';
			} else if (i == 2) {
				filename = 'Yaballe Export.csv';
			} else if (i == 3) {
				filename = 'Infinii Export.csv';
			} else if (i == 4) {
				filename = 'Priceyak Export.csv';
			} else if (i == 5) {
				filename = 'SKU Grid Export.csv';
			// } else if (i == 6) {
			// 	filename = 'AutoDS Export.csv';
			} else if (i == 7) {
                filename = 'Webseller Guru Export.csv';
            } else if (i == 8) {
                filename = 'ShopMaster Export.csv';
            } else {
				filename = 'export.csv';
			}

			if (!csv.match(/^data:text\/csv/i)) {
				// csv = 'data:text/csv;charset=utf-8,' + csv;
				// csv = 'data:text/csv;charset=UTF-8,\uFEFF' + csv;
				csv = 'data:text/csv;\uFEFF' + csv;
			}
			data = encodeURI(csv);

			link = document.createElement('a');
			link.setAttribute('href', data);
			link.setAttribute('download', filename);
			link.click();
		}
	}
}

// Get the products from saved local storage on every page refresh / new page.
function getProductList(){
	chrome.storage.local.get('rivalsProductList', function (items) {
		if (items.rivalsProductList != null) {
			rivalsProductList = items.rivalsProductList;
			var table = document.getElementById("grabberTable").getElementsByTagName('tbody')[0];
			for (i=0;i<rivalsProductList.length;i++){
				// console.log(rivalsProductList[i]['Item ID']);
				var row = table.insertRow(-1);
				var cell1 = row.insertCell(0).innerHTML = '<p>'+rivalsProductList[i]['Item ID']+'<p>';
			}
			$('#counter').get(0).innerText="Products: "+rivalsProductList.length;
		}
	});
	chrome.storage.local.get('rivalsProductListExport', function (items) {
		if (items.rivalsProductListExport != null) {
			rivalsProductListExport = items.rivalsProductListExport;
		}
	});
}

// Gets the source site on page load to assign "Grab Products" button the correct function for each site.
function getSourceSite(){
	if (document.URL.includes('dsmtool')) {
		sourceSite = 'DSM';
	} else if (document.URL.includes('easync')) {
		sourceSite = 'Easync';
	} else if (document.URL.includes('yaballe')) {
		sourceSite = 'Yaballe';
	} else if (document.URL.includes('infinii')) {
		sourceSite = 'Infinii';
	} else if (document.URL.includes('priceyak')) {
		sourceSite = 'Priceyak';
	} else if (document.URL.includes('skugrid')) {
		sourceSite = 'SKUGrid';
	// } else if (document.URL.includes('autods')) {
	// 	sourceSite = 'AutoDS';
	} else if (document.URL.includes('webseller')) {
		sourceSite = 'WebsellerGuru';
	} else if (document.URL.includes('shopmaster')) {
        sourceSite = 'ShopMaster';
    }
	console.log('sourceSite: ' + sourceSite);
}

// Extract ASIN by given URL. Check for regexes, matching the source site. return source and prooductID as list ( [source, productId] )
function hrefExtract(href){
    console.log('href = '+ href);
	if (href.toLowerCase().includes('hotlink')) {
		if (href.toLowerCase().includes('amazon.co.uk')) {
			source = 'amazon_co_uk';
			productId = href.match("([A-Z0-9]{12})");
		} else if (href.toLowerCase().includes('amazon.com')) {
			source = 'amazon_us';
			productId = href.match("([A-Z0-9]{12})");
		} else if (href.toLowerCase().includes('amazon.de')) {
            source = 'amazon_de';
            productId = href.match("([A-Z0-9]{12})");
        } else if (href.toLowerCase().includes('amazon.it')) {
            source = 'amazon_it';
            productId = href.match("([A-Z0-9]{12})");
        } else if (href.toLowerCase().includes('amazon.fr')) {
            source = 'amazon_fr';
            productId = href.match("([A-Z0-9]{12})");
        }  else if (href.toLowerCase().includes('ebay.com')) {
			source = 'ebay_us';
			productId = href.match("([A-Z0-9]{14})");
		} else if (href.toLowerCase().includes('ebay.co.uk')) {
			source = 'ebay_co_uk';
			productId = href.match("([A-Z0-9]{14})");
		} else if (href.toLowerCase().includes('ebay.de')) {
            source = 'ebay_de';
            productId = href.match("([A-Z0-9]{14})");
        } else if (href.toLowerCase().includes('ebay.it')) {
            source = 'ebay_it';
            productId = href.match("([A-Z0-9]{14})");
        } else if (href.toLowerCase().includes('ebay.fr')) {
            source = 'ebay_fr';
            productId = href.match("([A-Z0-9]{14})");
        } else if (href.toLowerCase().includes('walmart')) {
			source = 'walmart';
			productId = href.match("([0-9]{7,10})");
		} else {
			source = null;
		}
		if (source != null) {
			if (productId != null) {
				productId = productId[0];
				if (source != 'walmart') {
					productId = productId.slice(2,);
				}
			}	
		}
	} else if (href.toLowerCase().includes('amazon.com')) {
		source = 'amazon_us';
		productId = href.match("([A-Z0-9]{10})")[0];
	} else if (href.toLowerCase().includes('amazon.co.uk')) {
		source = 'amazon_co_uk';
		productId = href.match("([A-Z0-9]{10})")[0];
	} else if (href.toLowerCase().includes('amazon.de')) {
        source = 'amazon_de';
        productId = href.match("([A-Z0-9]{10})")[0];
    } else if (href.toLowerCase().includes('amazon.it')) {
        source = 'amazon_it';
        productId = href.match("([A-Z0-9]{10})")[0];
    } else if (href.toLowerCase().includes('amazon.fr')) {
        source = 'amazon_fr';
        productId = href.match("([A-Z0-9]{10})")[0];
    } else if (href.toLowerCase().includes('walmart')) {
		source = 'walmart';
		productId = href.match("([0-9]{7,10})")[0];
	} else if (href.toLowerCase().includes('aliexpress')) {
		source = 'aliexpress';
		href = href.replace('1144945576');
		productId = href.match("([0-9]{10,11})")[0];

	} else if (href.toLowerCase().includes('ebay.com')) {
		source = 'ebay_us';
		productId = href.match("([0-9]{12})")[0];
	} else if (href.toLowerCase().includes('ebay.co.uk')) {
		source = 'ebay_co_uk';
		productId = href.match("([0-9]{12})")[0];
	} else if (href.toLowerCase().includes('ebay.de')) {
		source = 'ebay_de';
		productId = href.match("([0-9]{12})")[0];
	} else if (href.toLowerCase().includes('ebay.it')) {
        source = 'ebay_it';
        productId = href.match("([0-9]{12})")[0];
    } else if (href.toLowerCase().includes('ebay.fr')) {
        source = 'ebay_fr';
        productId = href.match("([0-9]{12})")[0];
    } else if (href.toLowerCase().includes('homedepot')) {
        source = 'homedepot';
        productId = href.match("([0-9]{9})")[0];
    }
    // else{
    //     source = '';
    //     productId = '';
    // } 
	return [source, productId];
}

/* Default vars
productListExport contains all the export asins data. It's written by this way just to remember easily which position is attached to whom.
Inserting new products is done by the place of the suppliers id. For example, inserting products to DSM list will be productListExport[0].push(var) - They are first in bulk grab function */
rivalsProductList = [];
rivalsProductListExport = [[],[],[],[],[],[],[],[],[]];
// Get source site and saved products list on page load
getSourceSite();
getProductList();

// Get grabber state - is it hidden? is it visible? and hide / display it by cookie value.
state = readCookie('divState');
if (state=='hidden') {
	createCookie('divState','hidden');
	console.log('set hidden');
	// $('.bigContainer').css('display', 'none');
} else if (state=='visible') {
	createCookie('divState','visible');
	console.log('set visible');
	$('.bigContainer').css('display', 'block');
}

// Also changes cookie value on logo click.
$('#grabberDS').click(function(){
	$(".bigContainer").toggle(1000, function(){
		state = readCookie('divState');
		if (state=='hidden') {
			createCookie('divState','visible');
		} else if (state=='visible') {
			createCookie('divState','hidden');
		} else {
			createCookie('divState','visible');
		}
	});
});

// Attach event to export button
$('#exportToDS').click(function(){
	downloadCSV(/*{ filename: sourceSite+" Export.csv" }*/);
});

// Delete button. Make sure to edit the productListExport variable when adding / removing a rival site list!!
$('#deleteGrab').click(function(){
	rivalsProductList = [];
	rivalsProductListExport = [[],[],[],[],[],[],[],[],[]];
	$('#grabberTable tbody tr').remove();
	chrome.storage.local.clear();
	$('#counter').get(0).innerText="Products: 0";
});

// Attaching events by source site to the Grab button
$('#bulkGrab').click(function(e){
	e.preventDefault();
    var shopmasterSource = false;
	if (sourceSite == 'DSM') {
	    // Define the table that contains all the data
		table = $('.table.table-striped.table-advanced.selling_table tbody');
		// Loop through table row, and in each get source asin (by url) and ebayID (by url)
		for (i=0;i<table.children().length;i++) {
			columns = table.children().eq(i).children();
			sourceHref = $(columns[5]).find('.expandable-content a').attr('href');
			ebayHref = columns[8].children[0].getAttribute('href');
			extractSourceList = hrefExtract(sourceHref);
			extractEbayList = hrefExtract(ebayHref);
			ebayItemId = extractEbayList[1];
			sourceId = extractSourceList[1];
			sourceProductSite = extractSourceList[0];
			ebaySite = extractEbayList[0];
			variationId = '';
			// Create an object containing all the data on that product.
			productObject = {
				'Item ID': ebayItemId,
				'Source ID': sourceId,
				'Source Site': sourceProductSite,
				'To Ebay': ebaySite,
				'Var SKU': '',
			};
			productObjectExport = {
				'Item ID': ebayItemId,
				'Source ID': sourceId
			};
			// Push data to lists, by list position! DSM is indexed as 0 in rivalsProductListExport, so it's pushed to 0!
			rivalsProductList.push(productObject);
			// rivalsProductListExport[0].push(productObjectExport);
			rivalsProductListExport[0].push(productObject);
		}
	// And goes on for every supplier
	} else if (sourceSite == 'Easync') {
		table = $('table.table.table-condenced.table-hover.listings-table.pages-table tbody');
		for (i=0;i<table.children().length;i++) {
			columns = table.children().eq(i).children();
			sourceHref = columns[3].children[0].children[0].children[0].children[0].getAttribute('href');
			ebayHref = columns[4].children[0].children[0].getAttribute('href');
			extractSourceList = hrefExtract(sourceHref);
			extractEbayList = hrefExtract(ebayHref);
			ebayItemId = extractEbayList[1];
			sourceId = extractSourceList[1];
			sourceProductSite = extractSourceList[0];
			ebaySite = extractEbayList[0];
			variationId = ' ';
			productObject = {
				'Item ID': ebayItemId,
				'Source ID': sourceId,
				'Source Site': sourceProductSite,
				'To Ebay': ebaySite,
				'Var SKU': '',
			};
			productObjectExport = {
				'Item ID': ebayItemId,
				'Source ID': sourceId
			};
			rivalsProductList.push(productObject);
			// rivalsProductListExport[0].push(productObjectExport);
			rivalsProductListExport[1].push(productObject);
		}
	} else if (sourceSite == 'Yaballe') {
		table = $('table.table.table-bordered.table-striped.table-condensed.table-hover.cf.monitor_table.ng-scope.ng-table tbody');
		for (i=0;i<table.children().length;i++) {
			columns = table.children().eq(i).children();
			sourceHref = columns[2].children[0].children[1].children[0].getAttribute('href');
			ebayHref = columns[3].children[0].children[0].getAttribute('href');
			extractSourceList = hrefExtract(sourceHref);
			extractEbayList = hrefExtract(ebayHref);
			ebayItemId = extractEbayList[1];
			sourceId = extractSourceList[1];
			sourceProductSite = extractSourceList[0];
			ebaySite = extractEbayList[0];
			variationId = '';
			productObject = {
				'Item ID': ebayItemId,
				'Source ID': sourceId,
				'Source Site': sourceProductSite,
				'To Ebay': ebaySite,
				'Var SKU': '',
			};
			productObjectExport = {
				'Item ID': ebayItemId,
				'Source ID': sourceId
			};
			rivalsProductList.push(productObject);
			// rivalsProductListExport[0].push(productObjectExport);
			rivalsProductListExport[2].push(productObject);
		}
	} else if (sourceSite == 'Infinii') {
		table = $('table.table.myptable.table-bordered.table-striped.tableebaylisting.dataTable.no-footer tbody');
		for (i=0;i<table.children().length;i++) {
			columns = table.children().eq(i).children();
			sourceHref = columns[1].children[0].getAttribute('href');
			ebayHref = columns[3].children[0].getAttribute('href');
			extractSourceList = hrefExtract(sourceHref);
			extractEbayList = hrefExtract(ebayHref);
			ebayItemId = extractEbayList[1];
			sourceId = extractSourceList[1];
			sourceProductSite = extractSourceList[0];
			ebaySite = extractEbayList[0];
			variationId = '';
			productObject = {
				'Item ID': ebayItemId,
				'Source ID': sourceId,
				'Source Site': sourceProductSite,
				'To Ebay': ebaySite,
				'Var SKU': '',
			};
			productObjectExport = {
				'Item ID': ebayItemId,
				'Source ID': sourceId
			};
			rivalsProductList.push(productObject);
			// rivalsProductListExport[0].push(productObjectExport);
			rivalsProductListExport[3].push(productObject);
		}
	} else if (sourceSite == 'Priceyak') {
		table = $('table.table.table-striped.table-condensed.table-hover tbody');
		for (i=1;i<table.children().length;i++) {
			columns = table.children().eq(i).children();
			sourceHref = columns[1].children[0].children[0].getAttribute('href');
			ebayHref = columns[2].children[0].children[0].getAttribute('href');
			extractSourceList = hrefExtract(sourceHref);
			extractEbayList = hrefExtract(ebayHref);
			ebayItemId = extractEbayList[1];
			sourceId = extractSourceList[1];
			sourceProductSite = extractSourceList[0];
			ebaySite = extractEbayList[0];
			variationId = '';
			productObject = {
				'Item ID': ebayItemId,
				'Source ID': sourceId,
				'Source Site': sourceProductSite,
				'To Ebay': ebaySite,
				'Var SKU': '',
			};
			productObjectExport = {
				'Item ID': ebayItemId,
				'Source ID': sourceId
			};
			rivalsProductList.push(productObject);
			// rivalsProductListExport[0].push(productObjectExport);
			rivalsProductListExport[4].push(productObject);
		}
	} else if (sourceSite == 'SKUGrid') {
		table = $('#ajaxGrid tbody');
		for (i=0;i<table.children().length;i++) {
			columns = table.children().eq(i).children();
			sourceHref = columns[7].children[0].getAttribute('href');
			try {
				ebayHref = columns[3].children[0].getAttribute('href');
			} catch (err) {
				ebayHref = null;
			}
			if (ebayHref != null) {
				extractSourceList = hrefExtract(sourceHref);
				extractEbayList = hrefExtract(ebayHref);
				ebayItemId = extractEbayList[1];
				sourceId = extractSourceList[1];
				sourceProductSite = extractSourceList[0];
				ebaySite = extractEbayList[0];
				variationId = '';
				if (sourceProductSite != null) {
					productObject = {
						'Item ID': ebayItemId,
						'Source ID': sourceId,
						'Source Site': sourceProductSite,
						'To Ebay': ebaySite,
						'Var SKU': '',
					};
					productObjectExport = {
						'Item ID': ebayItemId,
						'Source ID': sourceId
					};
					rivalsProductList.push(productObject);
					// rivalsProductListExport[0].push(productObjectExport);
					rivalsProductListExport[5].push(productObject);
				}
			}
		}
	// } else if (sourceSite == 'AutoDS') {
	// 	table = $('#products_table tbody');
	// 	for (i=0;i<table.children().length;i++) {
	// 		columns = table.children().eq(i).children();
	// 		sourceHref = columns[3].children[0].getAttribute('href');
	// 		try {
	// 			ebayHref = columns[2].children[0].getAttribute('href');
	// 		} catch (err) {
	// 			ebayHref = null;
	// 		}
	// 		if (ebayHref != null) {
	// 			extractSourceList = hrefExtract(sourceHref);
	// 			extractEbayList = hrefExtract(ebayHref);
	// 			ebayItemId = extractEbayList[1];
	// 			sourceId = extractSourceList[1];
	// 			sourceProductSite = extractSourceList[0];
	// 			ebaySite = extractEbayList[0];
	// 			variationId = '';
	// 			if (sourceProductSite != null) {
	// 				productObject = {
	// 					'Item ID': ebayItemId,
	// 					'Source ID': sourceId,
	// 					'Source Site': sourceProductSite,
	// 					'To Ebay': ebaySite,
	//  		 		'Var SKU': '',
	// 				};
	// 				productObjectExport = {
	// 					'Item ID': ebayItemId,
	// 					'Source ID': sourceId
	// 				};
	// 				rivalsProductList.push(productObject);
	// 				// rivalsProductListExport[0].push(productObjectExport);
	// 				rivalsProductListExport[6].push(productObject);
	// 			}
	// 		}
	// 	}
	} else if (sourceSite == 'WebsellerGuru') {
		table = $('table.table.table-striped.table-bordered.table-hover tbody');

		for (i=1;i<table.children().length;i=i+3) {
			columns = table.children().eq(i).children();
			sourceHref = columns[6].children[0].children[0].children[0].getAttribute('href');
			try {
				ebayHref = columns[3].children[0].getAttribute('href');
			} catch (err) {
				ebayHref = null;
			}
			if (ebayHref != null) {
				extractSourceList = hrefExtract(sourceHref);
				extractEbayList = hrefExtract(ebayHref);
				ebayItemId = extractEbayList[1];
				sourceId = extractSourceList[1];
				sourceProductSite = extractSourceList[0];
				ebaySite = extractEbayList[0];
				variationId = '';
				if (sourceProductSite != null) {
					productObject = {
						'Item ID': ebayItemId,
						'Source ID': sourceId,
						'Source Site': sourceProductSite,
						'To Ebay': ebaySite,
						'Var SKU': '',
					};
					productObjectExport = {
						'Item ID': ebayItemId,
						'Source ID': sourceId
					};
					rivalsProductList.push(productObject);
					rivalsProductListExport[7].push(productObject);
				}
			}
		}
	} else if (sourceSite == 'ShopMaster') {
        shopmasterSource = true;
        $(document).find('#content-spinner-small').show();
        $.when( shopmaster() ).then(
          function( status ) {
            writeGrabberTable();
            $(document).find('#content-spinner-small').hide();
          }
        );
    }
    if(!shopmasterSource){
        writeGrabberTable();
    }
});
