
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.task == "addProduct") {
        addProduct(request.data, sendResponse, request.map, request.link);
    } else if(request.task == "GETRequest"){
        var variationData = request.childAttr;
        var parent = request.parent;
        $.ajax({
            url : request.url,
            type : "GET",
            success : function(resp) {
                if(resp){
                    var allatrrArryWithID = [];
                    var attributeMainDiv = $(resp).find("#j-product-info-sku");
                    attributeMainDiv.find('.p-property-item').each(function(){
                        $(this).find('.p-item-main ul li').each(function(){
                            var attrText = $(this).find('a').text();
                            if(!attrText){
                                attrText = $(this).find('a').attr('title');
                            }
                            var attrId = $(this).find('a').attr('data-sku-id');
                            var attrTexRefined = attrText.trim().toLowerCase().replace(/\s+/g, "");
                            tempObject = {
                                'name': attrTexRefined,
                                'id': attrId
                            }
                            allatrrArryWithID.push(tempObject);
                        });
                    });
                    $(variationData).each(function(index,value){
                        variationData[index] = value.trim().toLowerCase().replace(/\s+/g, "");
                    });
                    var finalArray = []; 
                    
                    var proId = ''; 
                    $(variationData).each(function(index,value){
                        var attrArray = value.split('-');
                        var tempArray = []; 
                        var concanStr = '';
                        $(attrArray).each(function(index1,value1){
                            pos = allatrrArryWithID.map(function(e) { return e.name; }).indexOf(value1);
                            if(pos != -1){
                                tempArray.push(allatrrArryWithID[pos].id);
                            }else{
                                proId = value1;
                            }
                            
                        });
                        concanStr = proId+'-'+tempArray.join('-');
                        
                        finalArray.push(concanStr);
                    });
                    objTemp = {
                            'child' : finalArray,
                            'parent' : parent
                        }
                    var finalArray1 = [];
                    finalArray1.push(objTemp);
                    sendResponse(finalArray1);
                }else{
                    sendResponse(undefined);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                sendResponse(undefined);
            }
        });
        return true;
    }  else {
        console.error("Unknown request");
    }
    return true;
});
