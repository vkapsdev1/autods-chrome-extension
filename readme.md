# Short explanation
This extension is made for our users. It includes address copying & pasting, item grabber and an exporter from our rivals sites.

# How the code works?
## manifest.json - content scripts:
1. The first part is related to address copying ("myscript.js"), and to the "copy" buttons in suppliers pages.
2. The second part is related to the AutoDS Grabber to export asins from suppliers ("grabber.js").
3. The third part is related to the AutoDS Exporter, to export listings from rival monitors ("exporter.js").

## Grabber & Exporter:
Both code is built almost the same, same table, same functions, only the data and few functions were adjusted to match the export from each supplier / rival site.
The code injecting an HTML table & logo inside their own javascripts files. The grab button executes same pattern of functions for each supplier, but using different
divisions to get the products from. Appends all to list, each supplier have it's own list id (to allow grabbing from multiple suppliers and exportings all at once to different files).
Data is also saved on local storage, making it storaged until user deletes it / closing the chrome session.
**_The injected HTML table is also saved inside floatbar.html_**

### Short explanation by code flow:
1. The window itself that appears in the source site is define by var "table" in the first row. It's an injected html that everytime appends new information.
2. var grabber is the logo itself, that has events attached to it by id (open and close the window).
3. Some cookie functions, to save, read and erase cookies.
4. convertArrayOfObjectsToCSV & downloadCSV are copied functions from web. The only change you will need to customize is under downloadCSV function.
Every new supplier is saved to another array, and all are inside one array. for example:
var productListExport = [[],[],[]]
Each supplier has it's own array to seperate it's extracted listings from the other, to keep the ability of extracting from multiple sites and at the end export all together.
Just add an "else if" row, with the new wanted supplier.
5. Get source site - this is the function that determine the source site by it's URL, and assigns the value sourceSite. By that value the extension know which function to run
when extracting data.
6. getProductList - this function gets all the products from the local storage. This is required because that's how the extracted data saves over pages / refreshes etc. No need to touch.
7. Default vars - shows the suppliers by it's array id. Easier to remember. At the end it just pushes everything to productListExport.
8. Running get sourcesite on page load, and get products list if we had any saved product in local storage.
9. state reads the cookie state (if the exporter window was opened or closed), to keep that state on page load.
10. grabberDS click - means clicking on AutoDS icon. It toggles the display state (If it was visible to hidden, and the opposite) and also saves a cookie with the current state.
11. Export event attached to exportToDS button id.
12. Event attached to the delete button. MAKE SURE TO ADD AN ARRAY UNDER productListExport IF YOU ARE ADDING NEW SUPPLIER. This is cleaning the saved export list.
13. hrefExtract - this is the function that extracts asin + source site by URL. It matches the site regex pattern, and returns source and the item id inside a list.
14. This is the start of the grabbing function. bulkGrab is the id of the grab button. On click, it first determines which case should it run by, by sourceSite we defined on
page load.
14-2. It first loops by list of elements until it finds a match. These elements are the divisions in HTML that contains the data of search results / where the item should be
It is working that way to not grab also asins from "recommended for you" or any suggestions that are not related to your page search.
14-3. It gets all the links from that division.
14-4. running the hrefExtract function for every link, and pushing the returned item id to productID list.
14-5. Removing duplicates from the list.
14-6. Building the export object. For every product in list, creates an object with ID, and it's URL, and pushing it to the productListExport array.
14-7. Goes on for every source site. The things that changes are mostly the divisions, where to get the links from. Also noted the productsListExport.push method -
make sure it pushes the data to the wanted array (amazon us - array 0 inside that array. Amazon UK, array 1 inside that array etc. It's increasing to seperate asins)
15. After getting all the information, the few last rows are just appending it to the table we inserted on stage 1, so the user will be able to see it.
16. Also counting all elements and changing the inner text of it.
17. Save to storage, as I've explained earlier so we would have the data on page refresh / multiple suppliers pages.

**_There is also an explanation inside the javascript files themselves_**

## Address paster and copy buttons:
The code wasn't written by me, but as I saw, the address paster saves data from AutoDS site to local storage, and pastes it into source sites from local storage too.
Copy buttons are injected on page load, loops on active variations, and uses a simple clipboard copy function to copy their value.

# Obsufactoring
I am always encrypting the source code before uploading a new verison to the google chrome store. Chrome Extension can be easily copied, so this is a must for every release.
I use [javascriptobfuscator](https://javascriptobfuscator.com/downloads.aspx) for simple obfuscatoring. [This is what I tick](https://i.imgur.com/6iPEnoT.png) (By using the "Free plan" some functions are disabled)